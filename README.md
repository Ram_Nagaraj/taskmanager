Project Title
"taskmanager" is a java-based web application project to create tasks by a user.

Pre-requisites
"taskmanager" application is built using the following frameworks.

Server-Side Frameworks
1. Spring Boot (Uses Embedded Apache Tomcat Server)
2. Spring Security
3. Hibernate - (Java Persistence)
4. H2 - Embedded Database

Client-Side Frameworks
1. Thymeleaf Framework - Template Engine
2. HTML/CSS/JQuery/JavaScript

Tools Used
1. Git/BitBucket/SourceTree
2. Spring Suite IDE
3. Apache Maven

Installing
1. Git clone using "git clone https://Ram_Nagaraj@bitbucket.org/Ram_Nagaraj/taskmanager.git"
2. mvn clean install - This will build the project & execute the Unit Test Cases

Deployment
Once the the build is complete. Execute the following command in the target folder to launch the application.

java -jar taskmanager-1.0.jar 


Test Data
Username = "user/user2"
Password = "password/password"
