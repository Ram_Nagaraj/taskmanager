package com.treehouse.taskmanager.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.treehouse.taskmanager.model.Task;

@Repository
public interface TaskDao extends CrudRepository<Task, Long> {
    @Query("select t from Task t where t.user.id=:#{principal.id}")
    List<Task> findAll();
    
    public Task findByDescription(final String description);
    
    public Task findByLabel(final String label);
}
