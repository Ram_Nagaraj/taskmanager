package com.treehouse.taskmanager.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.treehouse.taskmanager.model.User;

@Repository
public interface UserDao extends CrudRepository<User,Long> {
    User findByUsername(String username);
}
