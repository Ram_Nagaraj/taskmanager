package com.treehouse.taskmanager.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.treehouse.taskmanager.model.User;

public interface UserService extends UserDetailsService {
    User findByUsername(String username);
}
