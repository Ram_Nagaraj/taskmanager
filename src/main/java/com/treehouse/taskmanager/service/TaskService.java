package com.treehouse.taskmanager.service;

import com.treehouse.taskmanager.model.Task;

public interface TaskService {
    public Iterable<Task> findAll();
    public Task findOne(Long id);
    public Task findByDescription(final String description);
    public Task findByLabel(final String label);
    public void toggleComplete(Long id);
    public void save(Task task);
}
