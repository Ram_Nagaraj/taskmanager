package com.treehouse.taskmanager.error.handler;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler({DataIntegrityViolationException.class})
	public ResponseEntity<?> handleException(DataIntegrityViolationException e) {
		return new ResponseEntity<>(HttpStatus.CONFLICT);
	}
}
