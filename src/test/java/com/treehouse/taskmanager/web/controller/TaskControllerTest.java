package com.treehouse.taskmanager.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.treehouse.Application;
import com.treehouse.taskmanager.model.Task;
import com.treehouse.taskmanager.service.TaskService;
import com.treehouse.taskmanager.service.UserService;


@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes=Application.class)
@TestPropertySource(locations="classpath:application-integrationtest.properties")
public class TaskControllerTest {
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private TaskService service;
	
	@MockBean
	private UserService userService;
	
	private Task mockTask;
	
	@Before
	public void setUp() {
		mockTask = new Task();
		mockTask.setDescription("Morning Walk");
		mockTask.setLabel("Personal");
		mockTask.setComplete(false);

		Mockito.when(service.findOne(new Long(1))).thenReturn(mockTask);
	}
	
	@Test
	public void createTask() throws Exception {
		
		/*TestRestTemplate testRestTemplate = new TestRestTemplate();
		ResponseEntity<String> response = testRestTemplate.withBasicAuth("user", "password").postForEntity("http://localhost:8080/tasks", "{\n" + 
				"	\"label\" 		:	\"Task\",\n" + 
				"	\"description\"	:	\"Night Walk\",\n" + 
				"	\"date\"			:	\"2017-11-16\"\n" + 
				"}",String.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));*/
		
		mockMvc.perform(put("/tasks")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\n" + 
						"	\"label\" 		:	\"Task\",\n" + 
						"	\"description\"	:	\"Night Walk\",\n" + 
						"	\"date\"			:	\"2017-11-16\"\n" + 
						"}"))
		.andExpect(status().is4xxClientError());
	}
	
	@Test
	public void markTaskComplete() throws Exception {
		/*mockMvc.perform(post("/marks")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\n" + 
						"	\"id\" 			:	\"1\",\n" + 
						"	\"description\"	:	\"Night Walk\",\n" + 
						"	\"complete\"		:	\"true\"\n" + 
						"}")).andExpect(status().isAccepted());*/
	}
	
	/*@Test 
	public void updateTask() throws Exception {
		mockMvc.perform(post("/task/update")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\n" + 
						"	\"label\" 		:	\"Task\",\n" + 
						"	\"description\"	:	\"Night Walk\",\n" + 
						"	\"date\"			:	\"2017-11-20\"\n" + 
						"}"))
		.andExpect(status().isOk());
	}
	
	@Test
	public void retrieveTask() throws Exception {
		mockMvc.perform(get("/task/retrieve")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.requestAttr("page", 0)
				.requestAttr("size", 2))
			   .andExpect(status().isOk());
	}
	
	@Test
	public void deleteTask() throws Exception {
		mockMvc.perform(delete("/task/delete/${1}")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}*/
}
