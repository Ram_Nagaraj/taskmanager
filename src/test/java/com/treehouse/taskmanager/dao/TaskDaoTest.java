package com.treehouse.taskmanager.dao;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.treehouse.taskmanager.model.Role;
import com.treehouse.taskmanager.model.Task;
import com.treehouse.taskmanager.model.User;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TaskDaoTest {
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private TaskDao taskDao;
	
	Task task;
	
	@Before
	public void before() {
		Role role = new Role();
		role.setName("ROLE_USER");
		entityManager.persist(role);
		entityManager.flush();
		
		User user = new User();
		user.setUsername("useruseruser");
		user.setPassword("$2a$08$wgwoMKfYl5AUE9QtP4OjheNkkSDoqDmFGjjPE2XTPLDe9xso/hy7u");
		user.setRole(role);
		//userDao.save(user);
		entityManager.persist(user);
		entityManager.flush();
		
		task = new Task();
		task.setDescription("Morning Walk");
		task.setComplete(false);
		task.setUser(user);
		taskDao.save(task);
		entityManager.persist(task);
		entityManager.flush();
	}
	
	@Test
	public void testCreateTask() {
		Task found = taskDao.findByDescription("Morning Walk");
		assertThat(found.getDescription().equals(task.getDescription()));
	}
	
	@Test
	public void testTaskForComplete() {
		task.setComplete(true);
		entityManager.persistAndFlush(task);
		
		Task found = taskDao.findByDescription("Morning Walk");
		Assertions.assertThat(found.isComplete());
	}
	
	@Test
	public void testTaskUpdate() {
		task.setLabel("Personal");
		entityManager.persistAndFlush(task);
		
		Task found = taskDao.findByLabel("Personal");
		Assertions.assertThat(found.getLabel().equals("Personal"));
	}
	
	@After
	public void after() {
		task = null;
		entityManager = null;
	}
}
