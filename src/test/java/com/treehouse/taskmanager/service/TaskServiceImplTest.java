package com.treehouse.taskmanager.service;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.treehouse.taskmanager.dao.TaskDao;
import com.treehouse.taskmanager.model.Task;

@RunWith(SpringRunner.class)
public class TaskServiceImplTest {
	@TestConfiguration
	static class TaskServiceTestContextConfiguration {
		
		@Bean
		public TaskServiceImpl taskService() {
			return new TaskServiceImpl();
		}
	}
	
	@Autowired
	private TaskServiceImpl taskService;
	
	@MockBean
	private TaskDao taskDao;
	
	private Task mockTask;
	
	@Before
	public void setUp() {
		mockTask = new Task();
		mockTask.setDescription("Morning Walk");
		mockTask.setLabel("Personal");
		mockTask.setComplete(true);
		Mockito.when(taskDao.findByDescription(mockTask.getDescription())).thenReturn(mockTask);
		Mockito.when(taskDao.findByLabel(mockTask.getLabel())).thenReturn(mockTask);
	}
	
	@Test
	public void testTaskDescription() {
		String description = "Morning Walk";
		Task returnedTask = taskService.findByDescription(description);
		Assertions.assertThat(mockTask.getDescription().equals(returnedTask.getDescription()));
	}
	
	@Test
	public void testFindByLabel() {
		String label = "Personal";
		Task returnedTask = taskService.findByLabel(label);
		Assertions.assertThat(mockTask.getLabel().equals(returnedTask.getLabel()));
	}
	
	@After
	public void destroy() {
		mockTask = null;
		taskDao = null;
		taskService = null;
	}
}
